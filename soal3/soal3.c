#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>

void makedir(char *path){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
    } else {
        ((wait(&status)) > 0);
    }
}

void unzip(char *path){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"unzip", "-j", "animal.zip",  "*.jpg", "-d", path, NULL};
        execv("/bin/unzip", argv);
    } else {
        ((wait(&status)) > 0);
    }
}

void copy(char *src, char *path){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"cp", "-n", src, path, NULL};
        execv("/bin/cp", argv);
    } else {
        ((wait(&status)) > 0);
    }
}


void delete(char *file){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"rm", "-d", file, NULL};
        execv("/bin/rm", argv);
    } else {
        ((wait(&status)) > 0);
    }
}

void evaluation(char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, "darat") == 0 || strcmp(dp->d_name, "air") == 0 || 
        strcmp(dp->d_name, "animal.zip") == 0 || strcmp(dp->d_name, ".") == 0 ||
        strcmp(dp->d_name, "..") == 0 ) continue;
        
        if(strstr(dp->d_name, "darat")){
            char dest[1000] = "/home/azhar/modul2/darat/";
            char src[1000] = "/home/azhar/modul2/";
            strcat(dest, dp->d_name);
            strcat(src, dp->d_name);
            copy(src, dest);
        }

        if(strstr(dp->d_name, "air")){
            char dest[1000] = "/home/azhar/modul2/air/";
            char src[1000] = "/home/azhar/modul2/";
            strcat(dest, dp->d_name);
            strcat(src, dp->d_name);
            copy(src, dest);
        }
        
        char del[1000] = "/home/azhar/modul2/";
        strcat(del, dp->d_name);
        delete(del);
    }
}

void delete_bird(char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);
    
    while((dp = readdir(dir)) != NULL){
        if(strstr(dp->d_name, "bird")){
            char del[100] = "/home/azhar/modul2/darat/";
            strcat(del, dp->d_name);
            delete(del);
        }
    }
}

void list(char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);
    char list[100];
    strcpy(list, path);
    strcat(list, "list.txt");
    FILE *fp = fopen(list, "a");
    struct stat fs;
    struct stat info;
    int r;
    int p;

    p = stat(path, &fs);
    if(p == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
    }

    r = stat(path, &info);
    if(r == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
    }

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0 ||
            strcmp(dp->d_name, "list.txt") == 0 ) continue;
        
        char owner[10];
        char per_r[10];
        char per_w[10];
        char per_x[10];

        struct passwd *pw = getpwuid(info.st_uid);
        if(pw != 0) strcpy(owner, pw->pw_name);

        if(fs.st_mode & S_IRUSR) strcpy(per_r, "r");
        if(fs.st_mode & S_IWUSR) strcpy(per_w, "w");
        if(fs.st_mode & S_IXUSR) strcpy(per_x, "x");

        fprintf(fp, "%s_%s%s%s_%s\n", owner, per_r, per_w, per_x, dp->d_name);
    }
    fclose(fp);
}

int main(){
    makedir("/home/azhar/modul2/darat/");
    sleep(3);
    makedir("/home/azhar/modul2/air/");
    unzip("/home/azhar/modul2/");
    evaluation("/home/azhar/modul2/");
    delete_bird("/home/azhar/modul2/darat/");
    list("/home/azhar/modul2/air/");
}
