# soal-shift-sisop-modul-2-A05-2022

## Nomor 1

### Deskripsi Soal

Mas Refadi adalah seorang wibu gemink. Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan programnya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.

#### 1.A

##### Membuat directory dengan nama 'gacha_gacha'
Fork digunakan agar childnya akan mengeksekusi mkdir untuk membuat directory gacha_gacha.
```
pid_t id_child1 = fork();

if( id_child1 == 0)
{
    char *mkdir[] = {"mkdir", "-p", "gacha_gacha", NULL};
    execv("/bin/mkdir", mkdir);
}
```
##### Download file zip dari Google Drive
Kita akan mendownload file zip dengan menggunakan wget dan merename zip tersebut dengan genshin_characters.zip atau genshin_weapons.zip. Untuk memastikan proses benar-benar selesai, maka ditaruh sleep(5).
```
pid_t id_child2 = fork();

if( id_child2 == 0 )
{
    char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "genshin_characters.zip", NULL};
    execv("/bin/wget", args);
    sleep(2);
}

pid_t id_child3 = fork();

if( id_child3 == 0 )
{
    char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "genshin_weapon.zip", NULL};
    execv("/bin/wget", args);
    sleep(2);
}

sleep(5);
```
##### Unzip file yang telah didownload
Selanjutnya, kedua zip tersebut akan di ekstrak dan akan ada dua folder yaitu characters dan weapons. Untuk memastikan proses benar-benar selesai, maka ditaruh sleep(10).
```
pid_t id_child4 = fork();

if( id_child4 == 0 )
{
    execlp("unzip","unzip","-q","/home/adindazhr/genshin_characters.zip",NULL);
}

pid_t id_child5 = fork();

if( id_child5 == 0 )
{
    execlp("unzip","unzip","-q","/home/adindazhr/genshin_weapon.zip",NULL);
}

sleep(10);
```
#### 1.B, 1.C, 1.D

Disini, kami menuliskan file apa saja yang ada pada directory characters dan weapons tersebut dan menuliskannya pada text file genshin_characters.txt dan juga genshin_weapons.txt
```
pid_t id_child6 = fork();

if( id_child6 == 0 )
{
    char *args[]={"ls", "./characters", NULL};
    int fd = open("genshin_characters.txt", O_CREAT | O_TRUNC | O_RDWR, 0644);
    close(STDOUT_FILENO);
    dup2(fd, STDOUT_FILENO);
    execv("/bin/ls", args);
}

pid_t id_child7 = fork();

if( id_child7 == 0 )
{
    char *args[]={"ls", "./weapons", NULL};
    int fd = open("genshin_weapons.txt", O_CREAT | O_TRUNC | O_RDWR, 0644);
    close(STDOUT_FILENO);
    dup2(fd, STDOUT_FILENO);
    execv("/bin/ls", args);
}

sleep(5);
```
##### Membuat directory setiap jumlah gacha mod 90
Lalu, untuk iterasi, pertama kita declare primogems awal kita yaitu 79000 dan akan berkurang sebanyak 160 primogems setiap kali gacha
```
for(int primogems=79000; primogems/sekali_gacha > 0; primogems-=sekali_gacha)
```
Jika jumlah gacha di mod dengan 90 adalah 1, atau dengan kata lain ada pada jumlah gacha ke 1, 91, dst., maka akan dibuat suatu directory dengan format nama total_gacha_{jumlah-gacha}. Directory ini akan memuat file 90 gacha kedepan oleh karena itu setiap jumlah gacha di mod 1 maka gacha_90 adalah jumlah gacha ditambah dengan 1.
```
if(pity%90 == 1) //pity 1, 91, dll
{
    int gacha_90 = pity+89;
    char* curr_dir_temp = (char*) calloc(500, sizeof(char));
    strcpy(curr_dir_temp, dir);
    strcat(curr_dir_temp, "total_gacha_");
    char buffer[5];
    sprintf(buffer, "%d", gacha_90);
    strcat(curr_dir_temp, buffer);
    
    pid_t id_child11 = fork();
    if(id_child11 == 0)
    {
        char *mkdir[] = {"mkdir", "-p", curr_dir_temp , NULL};
        execv("/bin/mkdir", mkdir);
    }
    
    strcpy(curr_dir, curr_dir_temp);
    sleep(5);
}
```
##### Membuat file setiap jumlah gacha mod 10
Jika jumlah gacha di mod dengan 10 adalah 1, maka akan dibuat satu file txt dengan format nama {Hh:Mm:Ss}_gacha_{jumlah-gacha}. Dikarenakan soal meminta selisih detik setiap nama txt file adalah 1, maka setiap sebuah txt file dibuat detik akan bertambah 1.
```
if(pity%10 == 1) 
{
    int gacha_10 = pity+9;
    
    detik++;
	secondpassed++;
    printf("%d/n",detik);
    if(detik == 60)
    {
        detik = 0;
        menit++;
    }
    if(menit == 60)
    {
        menit = 0;
        jam++;
    }
    
    char* curr_file_temp = (char*) calloc(500, sizeof(char));
    strcpy(curr_file_temp, curr_dir);
    strcat(curr_file_temp, "/");
    if(jam<10)
    {
        strcat(curr_file_temp, "0");
    }
    char buffer[5];
    sprintf(buffer, "%d", jam);
    strcat(curr_file_temp, buffer);
    memset(buffer, 0, 5);
    if(menit<10)
    {
        strcat(curr_file_temp, "0");
    }
    sprintf(buffer, "%d", menit);
    strcat(curr_file_temp, buffer);
    memset(buffer, 0, 5);
    if(detik<10)
    {
        strcat(curr_file_temp, "0");
    }
    sprintf(buffer, "%d", detik);
    strcat(curr_file_temp, buffer);
    memset(buffer, 0, 5);
    strcat(curr_file_temp, "_gacha_");
    sprintf(buffer, "%d", gacha_10);
    strcat(curr_file_temp, buffer);
    memset(buffer, 0, 5);
    strcat(curr_file_temp, ".txt");
    
    pid_t id_child12 = fork();
    if(id_child12 == 0)
    {
        char *touch[] = {"touch", curr_file_temp, NULL};
        execv("/bin/touch", touch);
    }
    
    strcpy(curr_file, curr_file_temp);
}
```
##### Jika gacha ganjil maka akan diambil characters dan jika gacha genap maka akan diambil weapons
Jika jumlah gacha ganjil maka sistem akan melakukan shuf dan mengambil 1 baris secara random dari genshin_characters.txt. Begitu juga sebaliknya, jika jumlah gacha genap maka sistem akan melakukan shuf dan mengambil 1 baris secara random dari genshin_weapons.txt
```
pid_t id_child11 = fork();
if(pity%2==1)
{
    if(id_child11 == 0)
    {
        int fd = open("/home/adindazhr/gacha_temp.txt", O_CREAT | O_RDWR | O_APPEND, 0644);
        dup2(fd,1);
        close(fd);
        execlp("shuf","shuf","-n","1","/home/adindazhr/genshin_characters.txt",NULL);
    }
}
else
{
    if(id_child11 == 0)
        {
        int fd = open("/home/adindazhr/gacha_temp.txt", O_CREAT | O_RDWR | O_APPEND, 0644);
                    dup2(fd,1);
                    close(fd);
        execlp("shuf","shuf","-n","1","/home/adindazhr/genshin_weapons.txt",NULL);
        }
}
```
##### Menentukan path menuju file JSON
File gacha_temp.txt akan berisi hasil gacha dengan format {nama character atau weapon}.json. Oleh karena itu, file tersebut akan ditambahkan dengan path tambahan. Jika ganjil maka akan ditambahkan /home/adindazhr/characters/ dan jika genap maka akan ditambahkan /home/adindazhr/weapons/.
```
FILE* fp = fopen("/home/adindazhr/gacha_temp.txt", "r");
char filename[50];
char jsonfile[200];
int line=0;
while (fgets(filename,sizeof(filename), fp))
{
    
    line++;
    if(line == pity)
    {
        if(pity%2==1)
        {
            strcat(jsonfile, "/home/adindazhr/characters/");
            strcat(jsonfile, filename);
            printf("%s\n",jsonfile);
        }
        else
        {
            strcat(jsonfile, "/home/adindazhr/weapons/");
            strcat(jsonfile, filename);
            printf("%s\n",jsonfile);
        }
        break;
    }
}
fclose(fp);
```
##### Menuliskan gacha ke dalam file txt dengan format {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}
Lalu, name dan rarity dari file json gacha akan diambil dan dimasukkan dalam variabel name dan rarity. Lalu dituliskan ke dalam text file yang ada pada directory gacha_gacha
```
parsed_json = json_tokener_parse(buffer);
json_object_object_get_ex(parsed_json, "name", &name);
json_object_object_get_ex(parsed_json, "rarity", &rarity);

FILE* file_gacha = fopen(curr_file, "a");
if(pity%2==1)
{
    char type[]="characters";
    fprintf(file_gacha, "%d_%s_%s_%s_%d\n", pity, type, json_object_get_string(rarity), json_object_get_string(name), primogems-160);
}
else
{
    char type[]="weapons";
    fprintf(file_gacha, "%d_%s_%s_%s_%d\n", pity, type, json_object_get_string(rarity), json_object_get_string(name), primogems-160);
}
```
Menghilangkan isi dari jsonfile agar tidak segmentation fault
```
memset(jsonfile, 0, 200);
```
#### 1.E
##### Menjadwalkan proses dibawah untuk berjalan 3 jam setelah 30 Maret jam 04:44
Dikarenakan soal meminta kita untuk menjalankan proses 3 jam setelah 30 Maret jam 04:44. Maka detik yang telah berlalu untuk menggacha akan dikurangkan dengan 3 jam. Lalu, sleep selama wait tersebut.
```
int wait = (3 * 3600) - secondpassed;
    
sleep(wait);
```
##### Men-zip directory gacha_gacha dengan password satuduatiga dan nama zip not_safe_for_wibu
Zip directory gacha_gacha dengan password satuduatiga dan nama zip not_safe_for_wibu
```
pid_t child_id16 = fork();
if(child_id16 == 0)
{
    execlp("zip","zip","-r","--password","satuduatiga","not_safe_for_wibu.zip","gacha_gacha", NULL);
}
```
##### Menghilangkan directory gacha_gacha, characters, weapon dan file gacha_temp.txt
Menghapus directory characters
```
pid_t child_id17 = fork();
if(child_id17 == 0)
{
    execlp("rm","rm","-rf","/home/adindazhr/characters", NULL);
}
```
Menghapus directory weapons
```
pid_t child_id18 = fork();
if(child_id18 == 0)
{
    execlp("rm","rm","-rf","/home/adindazhr/weapons", NULL);
}
```
Menghapus directory gacha_gacha
```
pid_t child_id19 = fork();
if(child_id19 == 0)
{
    execlp("rm","rm","-rf","/home/adindazhr/gacha_gacha", NULL);
}
```  
Menghapus file text gacha_temp.txt
```
pid_t child_id20 = fork();
if(child_id20 == 0)
{
    execlp("rm","rm","/home/adindazhr/gacha_temp.txt", NULL);
}
```
## Soal 2

### Deskripsi SOal
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat
ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea
yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah
mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review,
tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan
poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan
untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun
untuk menyelesaikan pekerjaannya.

#### 2.A
Mengextract zip dan hanya mengambil file yang penting saja. Mengextract zip ke directory `/home/[USER]/shift2/drakor` lalu hanya di simpan file yang berekstensi `.png`.
Pertama kita membuat directory untuk menampung file unzip, lalu mengunzip zip file tersebut
```
    char *name[2] = {"/home/awan/shift2","/home/awan/shift2/drakor"};
  
  if ((child_id = fork()) == 0) {

    execlp("mkdir","mkdir", "-p",name[0], name[1] , NULL);

  } 
  while ((wait(&status)) > 0);
  if ((child_id = fork()) == 0) {
    char *argv[] ={"unzip", "-q", "/home/awan/praktikum_2/drakor.zip","*.png","-d", "/home/awan/shift2/drakor", NULL};
    execv("/usr/bin/unzip", argv);
  }
```
#### 2.B
Membuat kategori sesuai drakor yang ada. Pada fungsi makeCat maka dibuat kategori menggunakan bantuan `strtok`
```
strcpy(tmp1, dir->d_name); //reply1988;2015;comedy_nightmare-teacher;2016;horror.png
tok1 = strtok(tmp1,";"); //reply1988
strcpy(nama1, tok1);
        
tok1 = strtok(NULL,";"); //2015
strcpy(rilis1, tok1);
        
tok1 = strtok(NULL,"_");//comedy
strcpy(cat1, tok1);
```
menggunakan cara ini akan didapatkan nama drakor, tahun rilis drakor, dan kategori film drakor.

#### 2.C dan 2.D
Untuk memindahkan poster digunakan cara
```
if ((child_id = fork()) == 0) {
    execlp("cp","cp","-p", Files, drakor1, NULL);
}
else while((wait(&status)) > 0);

if ((child_id = fork()) == 0) {
    execlp("mv","mv","-f", Files, drakor2, NULL);
}
else while((wait(&status)) > 0);
```
### 2.E
Yang terakhir membuat list dari drakor yang ada di `data.txt`.
```
fptr = fopen(fname1, "ab+");
char q[100] = "nama : ";
strcat(q, nama1); 
strcat(q, "\nrilis  : "); 
strcat(q, " tahun ");
strcat(q, rilis1);
strcat(q, " \n\n");
puts(q, fptr);
fclose(fptr);
```

Semua program ini saya iterasi yang akan mengiterasi tiap file gambar yang ada pada folder drakor
```
struct dirent *dir;
DIR *file = opendir(src);

if(!file){
    return;
}

while((dir = readdir(file)) != NULL){
    ...
}
```
### Kendala
1. Segmentation Fault yang ada pada strcpy 

## Soal 3

### Deskripsi Soal
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan
tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang
banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

#### 3.A
Membuat 2 directory di `/home/[USER]/modul2/` dengan nama `darat` lalu 3 detik kemudian
membuat directory ke 2 dengan nama `air`.
fungsi main:
```
    makedir("/home/azhar/modul2/darat/");
    sleep(3);
    makedir("/home/azhar/modul2/air/");
```
setelah membuat directory `darat` panggil fungsi `sleep()` agar berhenti sejenak selama 3 detik.
fungsi makedir (membuat directory):
```
void makedir(char *path){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```

#### 3.B 
Extract file `animal.zip` yang berada di `/home/[USER]/modul2/`
fungsi main:
```
unzip("/home/azhar/modul2/");
```
fungsi unzip:
```
void unzip(char *path){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"unzip", "-j", "animal.zip",  "*.jpg", "-d", path, NULL};
        execv("/bin/unzip", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```

#### 3.C
Klarifikasikan hewan berdasarkan nama file lalu masukkan ke dalam folder yang sesuai.
pada fungsi klarifikasi memerlukan 2 fungsi lain yaitu copy dan delete
fungsi copy:
```
void copy(char *src, char *path){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"cp", "-n", src, path, NULL};
        execv("/bin/cp", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
fungsi delete:
```
void delete(char *file){
    int status; 
    pid_t child_id = fork();
    if(child_id == 0){
        char *argv[] = {"rm", "-d", file, NULL};
        execv("/bin/rm", argv);
    } else {
        ((wait(&status)) > 0);
    }
```
fungsi klarifikasi:
```
void evaluation(char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, "darat") == 0 || strcmp(dp->d_name, "air") == 0 || 
        strcmp(dp->d_name, "animal.zip") == 0 || strcmp(dp->d_name, ".") == 0 ||
        strcmp(dp->d_name, "..") == 0 ) continue;
        
        if(strstr(dp->d_name, "darat")){
            char dest[1000] = "/home/azhar/modul2/darat/";
            char src[1000] = "/home/azhar/modul2/";
            strcat(dest, dp->d_name);
            strcat(src, dp->d_name);
            copy(src, dest);
        }

        if(strstr(dp->d_name, "air")){
            char dest[1000] = "/home/azhar/modul2/air/";
            char src[1000] = "/home/azhar/modul2/";
            strcat(dest, dp->d_name);
            strcat(src, dp->d_name);
            copy(src, dest);
        }
        
        char del[1000] = "/home/azhar/modul2/";
        strcat(del, dp->d_name);
        delete(del);
    }
}
```
klarifikasi berdasarkan nama ini menggunakan `strstr` jadi apabila tidak ada yang 
keterangan nantinya akan dihapus tanpa di-copy.

#### 3.D
Menghapus semua nama file yang ada kata `bird`.
fungsi delete bird:
```
void delete_bird(char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);
    
    while((dp = readdir(dir)) != NULL){
        if(strstr(dp->d_name, "bird")){
            char del[100] = "/home/azhar/modul2/darat/";
            strcat(del, dp->d_name);
            delete(del);
        }
    }
}
```

#### 3.E
Membuat file list.txt yang berisi nama hewan dari folder air dengan format nama
`UID_[UID file permission]_Nama File.[jpg/png]`
fungsi list:
```
void list(char *path){
    struct dirent *dp;
    DIR *dir = opendir(path);
    char list[100];
    strcpy(list, path);
    strcat(list, "list.txt");
    FILE *fp = fopen(list, "a");
    struct stat fs;
    struct stat info;
    int r;
    int p;

    p = stat(path, &fs);
    if(p == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
    }

    r = stat(path, &info);
    if(r == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
    }

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0 ||
            strcmp(dp->d_name, "list.txt") == 0 ) continue;
        
        char owner[10];
        char per_r[10];
        char per_w[10];
        char per_x[10];

        struct passwd *pw = getpwuid(info.st_uid);
        if(pw != 0) strcpy(owner, pw->pw_name);

        if(fs.st_mode & IRUSR) strcpy(per_r, "r");
        if(fs.st_mode & IWUSR) strcpy(per_w, "w");
        if(fs.st_mode & IXUSR) strcpy(per_x, "x");

        fprintf(fp, "%s_%s%s%s_%s\n", owner, per_r, per_w, per_x, dp->d_name);
    }
    fclose(fp);
}
```
Untuk mencari nama user yang punya memakai
```
struct passwd *pw = getpwuid(info.st_uid);
if(pw != 0) strcpy(owner, pw->pw_name);
```
dan untuk mencari file permission-nya dengan cara:
```
if(fs.st_mode & S_IRUSR) strcpy(per_r, "r");
if(fs.st_mode & S_IWUSR) strcpy(per_w, "w");
if(fs.st_mode & S_IXUSR) strcpy(per_x, "x");
```
setelah itu di-print-kan ke file `list.txt`
```
fprintf(fp, "%s_%s%s%s_%s\n", owner, per_r, per_w, per_x, dp->d_name);
```
