#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
#include <json-c/json.h>

int main()
{
    int secondpassed=0;
    
    pid_t id_child1 = fork();

    if( id_child1 == 0)
    {
        char *mkdir[] = {"mkdir", "-p", "gacha_gacha", NULL};
        execv("/bin/mkdir", mkdir);
    }

    pid_t id_child2 = fork();

    if( id_child2 == 0 )
    {
        char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "genshin_characters.zip", NULL};
        execv("/bin/wget", args);
        sleep(2);
    }

    pid_t id_child3 = fork();

    if( id_child3 == 0 )
    {
        char *args[]={"wget", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "genshin_weapon.zip", NULL};
        execv("/bin/wget", args);
        sleep(2);
    }

    sleep(5);

    pid_t id_child4 = fork();

    if( id_child4 == 0 )
    {
		execlp("unzip","unzip","-q","/home/adindazhr/genshin_characters.zip",NULL);
    }

    pid_t id_child5 = fork();

    if( id_child5 == 0 )
    {
		execlp("unzip","unzip","-q","/home/adindazhr/genshin_weapon.zip",NULL);
    }

    sleep(10);

    pid_t id_child6 = fork();

    if( id_child6 == 0 )
    {
        char *args[]={"ls", "./characters", NULL};
        int fd = open("genshin_characters.txt", O_CREAT | O_TRUNC | O_RDWR, 0644);
		close(STDOUT_FILENO);
		dup2(fd, STDOUT_FILENO);
		execv("/bin/ls", args);
    }

    pid_t id_child7 = fork();

    if( id_child7 == 0 )
    {
		char *args[]={"ls", "./weapons", NULL};
        int fd = open("genshin_weapons.txt", O_CREAT | O_TRUNC | O_RDWR, 0644);
        close(STDOUT_FILENO);
        dup2(fd, STDOUT_FILENO);
        execv("/bin/ls", args);
    }

    sleep(5);

    int sekali_gacha = 160;
    
    int jam = 4;
    int menit = 44;
    int detik = 44;
    
    int pity = 0;
    
    int sisa_gacha;
    
    char dir[] = "/home/adindazhr/gacha_gacha/";
    char curr_dir[500];
    char curr_file[500];
    
    for(int primogems=79000; primogems/sekali_gacha > 0; primogems-=sekali_gacha)
    {
    	pity++;
    	if(pity%90 == 1) //pity 1, 91, dll
    	{
    		int gacha_90 = pity+89;
    		char* curr_dir_temp = (char*) calloc(500, sizeof(char));
    		strcpy(curr_dir_temp, dir);
    		strcat(curr_dir_temp, "total_gacha_");
    		char buffer[5];
    		sprintf(buffer, "%d", gacha_90);
    		strcat(curr_dir_temp, buffer);
    		
    		pid_t id_child11 = fork();
    		if(id_child11 == 0)
    		{
    			char *mkdir[] = {"mkdir", "-p", curr_dir_temp , NULL};
        		execv("/bin/mkdir", mkdir);
    		}
    		
    		strcpy(curr_dir, curr_dir_temp);
    		sleep(5);
    	}
    	if(pity%10 == 1) 
    	{
    		int gacha_10 = pity+9;
    		
    		detik++;
			secondpassed++;
			printf("%d/n",detik);
			if(detik == 60)
			{
				detik = 0;
				menit++;
			}
			if(menit == 60)
			{
				menit = 0;
				jam++;
			}
    		
    		char* curr_file_temp = (char*) calloc(500, sizeof(char));
    		strcpy(curr_file_temp, curr_dir);
    		strcat(curr_file_temp, "/");
    		if(jam<10)
    		{
    			strcat(curr_file_temp, "0");
    		}
    		char buffer[5];
    		sprintf(buffer, "%d", jam);
    		strcat(curr_file_temp, buffer);
    		memset(buffer, 0, 5);
    		if(menit<10)
    		{
    			strcat(curr_file_temp, "0");
    		}
    		sprintf(buffer, "%d", menit);
    		strcat(curr_file_temp, buffer);
    		memset(buffer, 0, 5);
    		if(detik<10)
    		{
    			strcat(curr_file_temp, "0");
    		}
    		sprintf(buffer, "%d", detik);
    		strcat(curr_file_temp, buffer);
    		memset(buffer, 0, 5);
    		strcat(curr_file_temp, "_gacha_");
    		sprintf(buffer, "%d", gacha_10);
    		strcat(curr_file_temp, buffer);
    		memset(buffer, 0, 5);
    		strcat(curr_file_temp, ".txt");
    		
    		pid_t id_child12 = fork();
    		if(id_child12 == 0)
    		{
    			char *touch[] = {"touch", curr_file_temp, NULL};
        		execv("/bin/touch", touch);
    		}
    		
    		strcpy(curr_file, curr_file_temp);
    	}
    	
    	pid_t id_child11 = fork();
        if(pity%2==1)
		{
			if(id_child11 == 0)
				{
				int fd = open("/home/adindazhr/gacha_temp.txt", O_CREAT | O_RDWR | O_APPEND, 0644);
				dup2(fd,1);
				close(fd);
				execlp("shuf","shuf","-n","1","/home/adindazhr/genshin_characters.txt",NULL);
				}
		}
		else
		{
			if(id_child11 == 0)
				{
				int fd = open("/home/adindazhr/gacha_temp.txt", O_CREAT | O_RDWR | O_APPEND, 0644);
							dup2(fd,1);
							close(fd);
				execlp("shuf","shuf","-n","1","/home/adindazhr/genshin_weapons.txt",NULL);
				}
		}
	
	sleep(2);
	
	FILE* fp = fopen("/home/adindazhr/gacha_temp.txt", "r");
	char filename[50];
	char jsonfile[200];
	int line=0;
	while (fgets(filename,sizeof(filename), fp))
	{
		
		line++;
		if(line == pity)
		{
			if(pity%2==1)
			{
				strcat(jsonfile, "/home/adindazhr/characters/");
				strcat(jsonfile, filename);
				printf("%s\n",jsonfile);
			}
			else
			{
				strcat(jsonfile, "/home/adindazhr/weapons/");
				strcat(jsonfile, filename);
				printf("%s\n",jsonfile);
			}
			break;
		}
	}
	fclose(fp);
	
	sleep(2);
	
	FILE *fptr;
	char buffer[5192];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;
	
	long int jsonfilesize = strlen(jsonfile);
	jsonfile[jsonfilesize - 1] = '\0';
	
	
	fptr = fopen(jsonfile, "r");
	fread(buffer, 5192, 1, fptr);
	fclose(fptr);
	
	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	
	FILE* file_gacha = fopen(curr_file, "a");
	if(pity%2==1)
	{
		char type[]="characters";
		fprintf(file_gacha, "%d_%s_%s_%s_%d\n", pity, type, json_object_get_string(rarity), json_object_get_string(name), primogems-160);
	}
	else
	{
		char type[]="weapons";
		fprintf(file_gacha, "%d_%s_%s_%s_%d\n", pity, type, json_object_get_string(rarity), json_object_get_string(name), primogems-160);
	}
	fclose(file_gacha);
	
	memset(jsonfile, 0, 200);
	
	
    	sleep(1);
    }

    int wait = (3 * 3600) - secondpassed;
    
    sleep(wait);
    
    pid_t child_id16 = fork();
    if(child_id16 == 0)
    {
    	execlp("zip","zip","-r","--password","satuduatiga","not_safe_for_wibu.zip","gacha_gacha", NULL);
    }
    sleep(5);
    pid_t child_id17 = fork();
    if(child_id17 == 0)
    {
		execlp("rm","rm","-rf","/home/adindazhr/characters", NULL);
    }

    pid_t child_id18 = fork();
    if(child_id18 == 0)
    {
		execlp("rm","rm","-rf","/home/adindazhr/weapons", NULL);
    }

    pid_t child_id19 = fork();
    if(child_id19 == 0)
    {
		execlp("rm","rm","-rf","/home/adindazhr/gacha_gacha", NULL);
    }  

    pid_t child_id20 = fork();
    if(child_id20 == 0)
    {
		execlp("rm","rm","/home/adindazhr/gacha_temp.txt", NULL);
    }
    
    return 0;

}
