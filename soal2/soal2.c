#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>

char* cut_four (char* s) {
    int n;
    int i;
    char* new;
    for (i = 0; s[i] != '\0'; i++);
    // length of the new string
    n = i - 4 + 1;
    if (n < 1)
        return NULL;
    new = (char*) malloc (n * sizeof(char));
    for (i = 0; i < n - 1; i++)
        new[i] = s[i];
    new[i] = '\0';
    return new;
}

void makeCat(char *src){
  struct dirent *dir;
  DIR *file = opendir(src);

  if(!file){
    return;
  }

  while((dir = readdir(file)) != NULL){
    if(strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
      
      int status;
      pid_t child_id;
      char drakor1[1000] = "/home/awan/shift2/drakor/";
      char drakor2[1000] = "/home/awan/shift2/drakor/";
      char NewDir1[1000] = "/home/awan/shift2/drakor/";
      char NewDir2[1000] = "/home/awan/shift2/drakor/";
      char *tok1, *tok2, *tok3;
      char cat1[1000],cat2[1000],cat3[1000], tmp1[1000], tmp2[1000], tmp3[1000], tmp4[1000], nama1[1000], nama2[1000], nama3[100], rilis1[1000], rilis2[1000], rilis3[100];
      
      char Files[1000] = "/home/awan/shift2/drakor/";
      strcat(Files, dir->d_name);
      
      FILE *fptr;
      char fname1[1000];
      char fname2[1000];
      char fname3[1000];
      // memset(fname1, 0, strlen(fname1));
      // memset(fname2, 0, strlen(fname2));
      // memset(fname3, 0, strlen(fname3));
      // memset(tmp1, 0, strlen(tmp1));
      // memset(tmp2, 0, strlen(tmp2));
      // memset(tmp3, 0, strlen(tmp3));
      // memset(tmp4, 0, strlen(tmp4));
      // memset(nama1, 0, strlen(nama1));
      // memset(nama2, 0, strlen(nama2));
      // memset(nama3, 0, strlen(nama3));
      // memset(rilis1, 0, strlen(rilis1));
      // memset(rilis2, 0, strlen(rilis2));
      // memset(rilis3, 0, strlen(rilis3));
      // memset(cat1, 0, strlen(cat1));
      // memset(cat2, 0, strlen(cat2));
      // memset(cat3, 0, strlen(cat3));

      if (strstr(dir->d_name, "_")){
        
        strcpy(tmp1, dir->d_name); //reply1988;2015;comedy_nightmare-teacher;2016;horror.png
        tok1 = strtok(tmp1,";"); //reply1988
        strcpy(nama1, tok1);
        
        tok1 = strtok(NULL,";"); //2015
        strcpy(rilis1, tok1);
        
        tok1 = strtok(NULL,"_");//comedy
        strcpy(cat1, tok1);
        
        strcpy(tmp2, dir->d_name); //reply1988;2015;comedy_nightmare-teacher;2016;horror.png
        tok2 = strtok(tmp2, "_"); //reply1988;2015;comedy
        tok2 = strtok(NULL, "_"); //nightmare-teacher;2016;horror.png
        strcpy(tmp4, tok2);
        tok2 = strtok(tmp4,";");//nightmare-teacher
        strcpy(nama2, tok2);

        tok2 = strtok(NULL, ";");//2016
        strcpy(rilis2, tok2);

        tok2 = strtok(NULL,";"); //horror.png
        strcpy(cat2, cut_four(tok2));
       
        strcat(drakor1, cat1);
        strcat(drakor1,"/");
        strcat(drakor1, tmp1);
        strcat(drakor1,".png");

        strcat(drakor2, cat2);
        strcat(drakor2,"/");
        strcat(drakor2, tmp4);
        strcat(drakor2,".png");

        strcat(NewDir1, cat1);
        strcpy(fname1, NewDir1);

        strcat(NewDir2, cat2);
        strcpy(fname2, NewDir2);

        strcat(fname1, "/data.txt");
        strcat(fname2, "/data.txt");

        if ((child_id = fork()) == 0) {
        execlp("mkdir","mkdir","-p", NewDir1, NULL);
        }

        while((wait(&status)) > 0);

        if ((child_id = fork()) == 0) {
        execlp("mkdir","mkdir","-p", NewDir2, NULL);
        }
        while((wait(&status)) > 0);

        if ((child_id = fork()) == 0) {
        execlp("cp","cp","-p", Files, drakor1, NULL);
        }
        else while((wait(&status)) > 0);

        if ((child_id = fork()) == 0) {
        execlp("mv","mv","-f", Files, drakor2, NULL);
        }
        else while((wait(&status)) > 0);

        fptr = fopen(fname1, "ab+");
        char q[100] = "nama : ";
        strcat(q, nama1); 
        strcat(q, "\nrilis  : "); 
        strcat(q, " tahun ");
        strcat(q, rilis1);
        strcat(q, " \n\n");
        fputs(q, fptr);
        fclose(fptr);

        fptr = fopen(fname2, "ab+");
        char p[100] = "nama : ";
        strcat(p, nama2); 
        strcat(p, "\nrilis  : "); 
        strcat(p, " tahun ");
        strcat(p, rilis2);
        strcat(p, " \n\n");
        fputs(p, fptr);
        fclose(fptr);
      }
        
      if(!(strstr(dir->d_name, "_"))) {
        
        strcpy(tmp3, dir->d_name); //fight-for-my-way;2017;comedy.png
        tok3 = strtok(tmp3,";"); //fight-for-my-way
        strcpy(nama3, tok3);
        
        tok3 = strtok(NULL,";"); //2017
        strcpy(rilis3, tok3);
        
        tok3 = strtok(NULL, ";"); //comedy.png
        
        strcpy(cat3, cut_four(tok3));
        
        strcat(drakor1, cat3);
        strcat(drakor1,"/");
        strcat(drakor1, tmp3);
        strcat(drakor1,".png");

        strcat(NewDir1, cat3);
        strcpy(fname3, NewDir1);
        strcat(fname3, "/data.txt");

        if ((child_id = fork()) == 0) {
        execlp("mkdir","mkdir","-p", NewDir1, NULL);
        }

        while((wait(&status)) > 0);

        if ((child_id = fork()) == 0) {
        char *argv[] = {"mv","-f", Files, drakor1, NULL};
        execv("/usr/bin/mv", argv);
        }
        else while((wait(&status)) > 0);

        fptr = fopen(fname3, "ab+");
        char p[1000] = "nama : ";
        strcat(p, nama3); 
        strcat(p, "\nrilis  : "); 
        strcat(p, " tahun ");
        strcat(p, rilis3);
        strcat(p, " \n\n");
        fputs(p, fptr);
        fclose(fptr);

      }

    }
  }
//   DIR *dp;
//   struct dirent *ep;
//   char paths[100] = "/home/awan/shift2/drakor";
//   struct drakor{
//     char name[100];
//     char year[10];
// }drk[100];


//   dp = opendir(paths);
//   if (dp != NULL){
//     while((ep = readdir(dp))){
//       if (ep->d_type == 4){
//         if (strstr(ep->d_name, "..") != NULL) continue;
//         if (strstr(ep->d_name, ".") != NULL) continue;
//           char datapaths[100];
//           char ch;
//           char name[100];
//           char year[100];
//           strcpy(datapaths, paths);
//           strcat(datapaths, "/");
//           strcat(datapaths, ep->d_name);
//           strcat(datapaths, "/data.txt");

//           FILE *readFile;
//           readFile = fopen(datapaths, "r");

//           int index = 0;
//           int line = 1; 
//           while(!feof(readFile)){
//             if (line%2 == 1){
//               fscanf(readFile, "%s", drk[index].name);
//                             // printf("\nName = %s\n", drk[index].name);
//             }
//             else{
//               fscanf(readFile, "%s", drk[index].year);
//                             // printf("\nYear = %s\n", drk[index].year);
//               index++;
//             }
//             line++;
//           }

//           for (int i = 0; i<index-1; i++){
//             for (int j = 0; j<index-i-1 ; j++){
//               int y1 = atoi(drk[j].year);
//               int y2 = atoi(drk[j+1].year);
//               if (y1 > y2){
//                 char temp[100];
//                 strcpy(temp, drk[j].year);
//                 strcpy(drk[j].year, drk[j+1].year);
//                 strcpy(drk[j+1].year, temp);

//                 strcpy(temp, drk[j].name);
//                 strcpy(drk[j].name, drk[j+1].name);
//                 strcpy(drk[j+1].name, temp);
//               }
//             }
//           }

                    

//           FILE *writeFile;
//           writeFile = fopen(datapaths, "w");
//           fclose(writeFile);
//           writeFile = fopen(datapaths, "a");

//           fprintf(writeFile, "Kategori: %s\n", ep->d_name);

//           for (int i = 0; i<index; i++){
//               fprintf(writeFile, "\nNama : %s\n", drk[i].name);
//               fprintf(writeFile, "Tahun : %s\n", drk[i].year);

//           }

//           fclose(readFile);
//           fclose(writeFile);
//       }
//     }
//   }
        
//         // Delete file
//   dp = opendir(paths);
//   if (dp != NULL){
//     while((ep = readdir(dp))){
//       if (ep->d_type == 4) continue;

//       char filepaths[100];

//       strcpy(filepaths, "/home/awan/shift2/drakor/");
//       strcat(filepaths, ep->d_name);

//       remove(filepaths);
//     }
//   }

  closedir(file);
}

int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  char *name[2] = {"/home/awan/shift2","/home/awan/shift2/drakor"};
  
  if ((child_id = fork()) == 0) {

    execlp("mkdir","mkdir", "-p",name[0], name[1] , NULL);

  } 
  while ((wait(&status)) > 0);
  if ((child_id = fork()) == 0) {
    char *argv[] ={"unzip", "-q", "/home/awan/praktikum_2/drakor.zip","*.png","-d", "/home/awan/shift2/drakor", NULL};
    execv("/usr/bin/unzip", argv);
  }
  sleep(15);

  makeCat("/home/awan/shift2/drakor/");
  
}